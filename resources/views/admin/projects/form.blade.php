@extends('twill::layouts.form', [
    'additionalFieldsets' => [
        ['fieldset' => 'image_grid', 'label' => 'Image Grid'],
        ['fieldset' => 'attributes', 'label' => 'Attributes'],
    ]
])

@section('contentFields')
    @formField('input', [
        'name' => 'overlay_title',
        'label' => 'Overlay Title',
        'maxlength' => 100,
        'required' => true,
    ])
    @formField('input', [
        'name' => 'client',
        'label' => 'Client',
        'maxlength' => 100,
        'required' => true,
    ])
    @formField('input', [
        'name' => 'description',
        'label' => 'Description',
        'type' => 'textarea',
        'required' => true,
    ])

    @formField('browser', [
        'max' => 1,
        'name' => 'highlighted_image',
        'moduleName' => 'gridSections',
        'label' => 'Highlighted Image',
        'note' => 'This is the image that appears directly below the header.' 
    ])

    @formField('browser', [
        'max' => 50,
        'name' => 'services',
        'moduleName' => 'services',
        'label' => 'Services'
    ])

    @formField('browser', [
        'max' => 50,
        'name' => 'collaborators',
        'moduleName' => 'collaborators',
        'label' => 'Collaborators'
    ])
@stop

@section('fieldsets')
	<a17-fieldset title="Image Grid" id="image_grid">
        @formField('browser', [
	    	'max' => 20,
            'name' => 'gridSections',
            'moduleName' => 'gridSections',
            'label' => 'Sections' 
	    ])
    </a17-fieldset>
    <a17-fieldset title="Attributes" id="attributes">
        
    </a17-fieldset>
@stop
