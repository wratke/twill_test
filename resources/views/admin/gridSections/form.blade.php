@extends('twill::layouts.form')

@section('contentFields')

	@formField('input', [
        'name' => 'title',
        'label' => 'Title',
        'required' => true
    ])

    @formField('select', [
	    'name' => 'section_type',
	    'label' => 'Section Layout',
	    'placeholder' => 'Select an layout',
	    'required' => true,
	    'options' => [
	        [
	            'value' => 'WorkImage-SingleLarge',
	            'label' => 'WorkImage-SingleLarge'
	        ],
	        [
	            'value' => 'WorkImage-Single_1_5H',
	            'label' => 'WorkImage-Single_1.5H'
	        ],
	        [
	            'value' => 'WorkImage-Single_2H',
	            'label' => 'WorkImage-Single_2H'
	        ],
	        [
	            'value' => 'WorkImage-TwoSquares',
	            'label' => 'WorkImage-TwoSquares'
	        ],
	        [
	            'value' => 'WorkImage-EightSquares',
	            'label' => 'WorkImage-EightSquares'
	        ],
	        [
	            'value' => 'WorkImage-FiveSquaresRight',
	            'label' => 'WorkImage-FiveSquaresRight'
	        ],
	        [
	            'value' => 'WorkImage-FiveSquaresLeft',
	            'label' => 'WorkImage-FiveSquaresLeft'
	        ],
	        [
	            'value' => 'WorkImage-Description',
	            'label' => 'WorkImage-Description'
	        ],
	    ]
	])

	@component('twill::partials.form.utils._connected_fields', [
	        'fieldName' => 'section_type',
	        'fieldValues' => 'WorkImage-Description',
	        'renderForBlocks' => false
	])
	    @formField('input', [
	        'name' => 'grid_title',
	        'label' => 'Section Title'
	    ])

	    @formField('input', [
	        'name' => 'grid_description',
	        'label' => 'Section Description',
	        'type' => 'textarea'
	    ])
	@endcomponent

	@component('twill::partials.form.utils._connected_fields', [
	        'fieldName' => 'section_type',
	        'fieldValues' => 'WorkImage-SingleLarge',
	        'renderForBlocks' => false
	])
	    @formField('medias', [
	        'name' => 'single_rectangle',
	        'label' => 'Rectangle Image',
	        'max' => 1,
	        'note' => 'Image should be exactly 3040px X 1487px.'
	    ])

	    @formField('medias', [
	        'name' => 'single_square',
	        'label' => 'Square Image',
	        'max' => 1,
	        'note' => 'Image should be exactly 1464px X 1464px.'
	    ])
	@endcomponent

	@component('twill::partials.form.utils._connected_fields', [
	        'fieldName' => 'section_type',
	        'fieldValues' => 'WorkImage-Single_1_5H',
	        'renderForBlocks' => false
	])
	    @formField('medias', [
	        'name' => 'single_1_5_rectangle',
	        'label' => 'Rectangle Image',
	        'max' => 1,
	        'note' => 'Image should be exactly 3040px X 2266px.'
	    ])

	    @formField('medias', [
	        'name' => 'single_1_5_square',
	        'label' => 'Square Image',
	        'max' => 1,
	        'note' => 'Image should be exactly 1464px X 1464px.'
	    ])
	@endcomponent

	@component('twill::partials.form.utils._connected_fields', [
	        'fieldName' => 'section_type',
	        'fieldValues' => 'WorkImage-Single_2H',
	        'renderForBlocks' => false
	])
	    @formField('medias', [
	        'name' => 'single_double_height',
	        'label' => 'Rectangle Image',
	        'max' => 1,
	        'note' => 'Image should be exactly 3040px X 3040px.'
	    ])
	@endcomponent

	@component('twill::partials.form.utils._connected_fields', [
	        'fieldName' => 'section_type',
	        'fieldValues' => 'WorkImage-TwoSquares',
	        'renderForBlocks' => false
	])
	    @formField('medias', [
	        'name' => 'double_left',
	        'label' => 'Left/Top Image',
	        'max' => 1,
	        'note' => 'Image should be exactly 1464px X 1464px.'
	    ])

	    @formField('medias', [
	        'name' => 'double_right',
	        'label' => 'Right/Bottom Image',
	        'max' => 1,
	        'note' => 'Image should be exactly 1464px X 1464px. '
	    ])
	@endcomponent

	@component('twill::partials.form.utils._connected_fields', [
	        'fieldName' => 'section_type',
	        'fieldValues' => 'WorkImage-EightSquares',
	        'renderForBlocks' => false
	])
	    @formField('medias', [
	        'name' => 'eight1',
	        'label' => 'Image 1',
	        'max' => 1,
	        'note' => 'Image should be at minimum 732px X 732px.'
	    ])

	    @formField('medias', [
	        'name' => 'eight2',
	        'label' => 'Image 2',
	        'max' => 1,
	        'note' => 'Image should be at minimum 732px X 732px.'
	    ])

	    @formField('medias', [
	        'name' => 'eight3',
	        'label' => 'Image 3',
	        'max' => 1,
	        'note' => 'Image should be at minimum 732px X 732px.'
	    ])

	    @formField('medias', [
	        'name' => 'eight4',
	        'label' => 'Image 4',
	        'max' => 1,
	        'note' => 'Image should be at minimum 732px X 732px.'
	    ])

	    @formField('medias', [
	        'name' => 'eight5',
	        'label' => 'Image 5',
	        'max' => 1,
	        'note' => 'Image should be at minimum 732px X 732px.'
	    ])

	    @formField('medias', [
	        'name' => 'eight6',
	        'label' => 'Image 6',
	        'max' => 1,
	        'note' => 'Image should be at minimum 732px X 732px.'
	    ])

	    @formField('medias', [
	        'name' => 'eight7',
	        'label' => 'Image 7',
	        'max' => 1,
	        'note' => 'Image should be at minimum 732px X 732px.'
	    ])

	    @formField('medias', [
	        'name' => 'eight8',
	        'label' => 'Image 8',
	        'max' => 1,
	        'note' => 'Image should be at minimum 732px X 732px.'
	    ])
	@endcomponent

	@component('twill::partials.form.utils._connected_fields', [
	        'fieldName' => 'section_type',
	        'fieldValues' => 'WorkImage-FiveSquaresRight',
	        'renderForBlocks' => false
	])
	    @formField('medias', [
	        'name' => 'five_large_right',
	        'label' => 'Large Image',
	        'max' => 1,
	        'note' => 'Image should be exactly 1464px X 1464px.'
	    ])

	    @formField('medias', [
	        'name' => 'five_small1_right',
	        'label' => 'Small Image Top Left',
	        'max' => 1,
	        'note' => 'Image should be exactly 1464px X 1464px.'
	    ])

	    @formField('medias', [
	        'name' => 'five_small2_right',
	        'label' => 'Small Image Top Right',
	        'max' => 1,
	        'note' => 'Image should be exactly 1464px X 1464px.'
	    ])

	    @formField('medias', [
	        'name' => 'five_small3_right',
	        'label' => 'Small Image Bottom Left',
	        'max' => 1,
	        'note' => 'Image should be exactly 1464px X 1464px.'
	    ])

	    @formField('medias', [
	        'name' => 'five_small4_right',
	        'label' => 'Small Image Bottom Right',
	        'max' => 1,
	        'note' => 'Image should be exactly 1464px X 1464px.'
	    ])
	@endcomponent

	@component('twill::partials.form.utils._connected_fields', [
	        'fieldName' => 'section_type',
	        'fieldValues' => 'WorkImage-FiveSquaresLeft',
	        'renderForBlocks' => false
	])
	    @formField('medias', [
	        'name' => 'five_large_left',
	        'label' => 'Large Image',
	        'max' => 1,
	        'note' => 'Image should be exactly 1464px X 1464px.'
	    ])

	    @formField('medias', [
	        'name' => 'five_small1_left',
	        'label' => 'Small Image Top Left',
	        'max' => 1,
	        'note' => 'Image should be exactly 1464px X 1464px.'
	    ])

	    @formField('medias', [
	        'name' => 'five_small2_left',
	        'label' => 'Small Image Top Right',
	        'max' => 1,
	        'note' => 'Image should be exactly 1464px X 1464px.'
	    ])

	    @formField('medias', [
	        'name' => 'five_small3_left',
	        'label' => 'Small Image Bottom Left',
	        'max' => 1,
	        'note' => 'Image should be exactly 1464px X 1464px.'
	    ])

	    @formField('medias', [
	        'name' => 'five_small4_left',
	        'label' => 'Small Image Bottom Right',
	        'max' => 1,
	        'note' => 'Image should be exactly 1464px X 1464px.'
	    ])
	@endcomponent
@stop
