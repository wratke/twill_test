@extends('twill::layouts.form')

@section('contentFields')
    @formField('input', [
        'name' => 'title',
        'label' => 'Title',
        'maxlength' => 200
    ])

    @formField('hidden', [
	    'name' => 'type',
	    'options' => [
	        [
	            'value' => 'home',
	            'label' => 'home'
	        ],
	        [
	            'value' => 'work',
	            'label' => 'work'
	        ],
	    ]
	])

	
@stop

@section('fieldsets')
	@include('admin.pages.home')
	@include('admin.pages.work')
@stop