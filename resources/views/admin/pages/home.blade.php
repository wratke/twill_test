
@component('twill::partials.form.utils._connected_fields', [
        'fieldName' => 'type',
        'fieldValues' => 'home',
        'renderForBlocks' => false
])

	<a17-fieldset title="Header" id="header">
		@formField('medias', [
	        'name' => 'header_img',
	        'label' => 'Header Image',
	        'note' => 'Image should be 1520 X 1472. Optional.',
	        'max' => 1
	    ])

	    @formField('input', [
        'name' => 'text0',
        'label' => 'Header Text',
        'type' => 'textarea'
    ])
	</a17-fieldset>
	<a17-fieldset title="Recent Work" id="recent_work">
	    
	</a17-fieldset>
	<a17-fieldset title="Core Services" id="core_services">
	    
	</a17-fieldset>
	<a17-fieldset title="Clients" id="clients">
	    
	</a17-fieldset>
	<a17-fieldset title="Modules" id="modules">
	    
	</a17-fieldset>
	<a17-fieldset title="Contact" id="contact">
	    
	</a17-fieldset>
@endcomponent

