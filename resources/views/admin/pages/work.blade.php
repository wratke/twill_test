@component('twill::partials.form.utils._connected_fields', [
        'fieldName' => 'type',
        'fieldValues' => 'work',
        'renderForBlocks' => false
])
    @formField('input', [
        'name' => 'grid_title',
        'label' => 'S'
    ])

    @formField('input', [
        'name' => 'grid_description',
        'label' => 'Description',
        'type' => 'textarea'
    ])
@endcomponent