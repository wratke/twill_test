<?php

return [
    'home' => [
        'title' => 'Home',
        'raw' => true,
        'route' => '/cp/home/pages/1/edit',
        'primary_navigation' => [
            'home' => [
                'title' => 'Home',
                'module' => false,
                'raw' => true,
                'route' => '/cp/home/pages/1/edit'
            ],
            'clientLogos' => [
                'title' => 'Client Logos',
                'module' => true,
            ],
        ],
    ],
    'work' => [
        'title' => 'Work',
        'raw' => true,
        'route' => '/cp/work/pages/2/edit',
        'primary_navigation' => [
            'work' => [
                'title' => 'Work',
                'module' => false,
                'raw' => true,
                'route' => '/cp/work/pages/2/edit'
            ],
            'projects' => [
                'title' => 'Projects',
                'module' => true,
            ],
            'gridSections' => [
                'title' => 'Single Project Grid Section',
                'module' => true,
            ],
           'services' => [
                'title' => 'Services',
                'module' => true,
            ],
            'collaborators' => [
                'title' => 'Collaborators',
                'module' => true,
            ], 
        ],
    ],
    'services' => [
        'title' => 'Services',
        'raw' => true,
        'route' => '/cp/work/pages/3/edit',
        'primary_navigation' => [
            'services' => [
                'title' => 'Services',
                'module' => false,
                'raw' => true,
                'route' => '/cp/work/pages/3/edit'
            ],
        ],
    ],
    'about' => [
        'title' => 'About',
        'raw' => true,
        'route' => '/cp/work/pages/4/edit',
        'primary_navigation' => [
            'about' => [
                'title' => 'About',
                'module' => false,
                'raw' => true,
                'route' => '/cp/work/pages/4/edit'
            ],
        ],
    ],
    /*
    'modules' => [
        'title' => 'Modules',
        'route' => 'modules.index',
        'primary_navigation' => [
            'about' => [
                'title' => 'About',
                'module' => false,
                'raw' => true,
                'route' => '/cp/work/pages/4/edit'
            ],
        ],
    ],*/
];
