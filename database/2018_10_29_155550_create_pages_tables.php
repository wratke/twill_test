<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePagesTables extends Migration
{
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            
            $table->string('header_img', 300)->nullable();
            
            // your generated model and form include a description field, to get you started, but feel free to get rid of it if you don't need it
            

            $table->string('single_img_top', 300)->nullable();
            $table->string('single_img_link', 300)->nullable();
            $table->string('single_rectangle', 300)->nullable();
            $table->string('single_square', 300)->nullable();

            $table->string('five_large_top', 300)->nullable();
            $table->text('five_large_link')->nullable();
            $table->string('five_small1', 300)->nullable();
            $table->text('five_small1_link')->nullable();
            $table->string('five_small2', 300)->nullable();
            $table->text('five_small2_link')->nullable();
            $table->string('five_small3', 300)->nullable();
            $table->text('five_small3_link')->nullable();
            $table->string('five_small4', 300)->nullable();
            $table->text('five_small4_link')->nullable();
            
            $table->string('service_img1', 300)->nullable();
            $table->string('small_service_img1', 300)->nullable();

            
            $table->string('service_img2', 300)->nullable();
            $table->string('small_service_img2', 300)->nullable();

            
            $table->string('service_img3', 300)->nullable();
            $table->string('small_service_img3', 300)->nullable();

            
            $table->string('service_img4', 300)->nullable();
            $table->string('small_service_img4', 300)->nullable();

            
            $table->string('service_img5', 300)->nullable();
            $table->string('small_service_img5', 300)->nullable();
        });

        // remove this if you're not going to use any translated field, ie. using the HasTranslation trait. If you do use it, create fields you want translatable in this table instead of the main table above. You do not need to create fields in both tables.
        Schema::create('page_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'page');

            $table->text('description')->nullable();
            $table->text('header_words')->nullable();

            $table->string('recent_work_section_title', 300)->nullable();
            $table->text('recent_work_section_description')->nullable();

            $table->text('single_img_text')->nullable();
            $table->text('five_large_text')->nullable();
            $table->text('five_small1_text')->nullable();
            $table->text('five_small2_text')->nullable();
            $table->text('five_small3_text')->nullable();
            $table->text('five_small4_text')->nullable();

            $table->string('core_services_section_title', 300)->nullable();
            $table->text('core_services_section_description')->nullable();

            $table->string('service_title1', 300)->nullable();
            $table->text('service_description1')->nullable();

            $table->string('service_title2', 300)->nullable();
            $table->text('service_description2')->nullable();

            $table->string('service_title3', 300)->nullable();
            $table->text('service_description3')->nullable();
            
            $table->string('service_title4', 300)->nullable();
            $table->text('service_description4')->nullable();

            $table->string('service_title5', 300)->nullable();
            $table->text('service_description5')->nullable();

            $table->string('client_section_title', 300)->nullable();
            $table->text('client_description')->nullable();

            $table->string('contact_section_title', 300)->nullable();
            $table->text('contact_description')->nullable();
        });

        // remove this if you're not going to use slugs, ie. using the HasSlug trait
        Schema::create('page_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'page');
        });

        // remove this if you're not going to use revisions, ie. using the HasRevisions trait
        Schema::create('page_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'page');
        });
    }

    public function down()
    {
        Schema::dropIfExists('page_revisions');
        Schema::dropIfExists('page_translations');
        Schema::dropIfExists('page_slugs');
        Schema::dropIfExists('pages');
    }
}
