<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGridSectionsTables extends Migration
{
    public function up()
    {
        Schema::create('grid_sections', function (Blueprint $table) {
            
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            
            // feel free to modify the name of this column, but title is supported by default (you would need to specify the name of the column Twill should consider as your "title" column in your module controller if you change it)
            $table->string('title', 200)->nullable();
            
            // your generated model and form include a description field, to get you started, but feel free to get rid of it if you don't need it
            $table->string('section_type', 60)->nullable();

            // text section
            $table->string('section_title', 200)->nullable(); 
            $table->text('section_description')->nullable();


            // add those 2 colums to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();


            // use this column with the HasPosition trait
            // $table->integer('position')->unsigned()->nullable();
        });

        // remove this if you're not going to use slugs, ie. using the HasSlug trait
        Schema::create('grid_section_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'grid_section');
        });

        // remove this if you're not going to use revisions, ie. using the HasRevisions trait
        Schema::create('grid_section_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'grid_section');
        });
    }

    public function down()
    {
        Schema::dropIfExists('grid_section_revisions');
        Schema::dropIfExists('grid_section_translations');
        Schema::dropIfExists('grid_section_slugs');
        Schema::dropIfExists('grid_sections');
    }
}
