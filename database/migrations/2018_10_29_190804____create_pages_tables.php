<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePagesTables extends Migration
{
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);

            $table->string('title', 200)->nullable();
            $table->string('type', 20)->nullable();
            
            // feel free to modify the name of this column, but title is supported by default (you would need to specify the name of the column Twill should consider as your "title" column in your module controller if you change it)
            $table->string('image0', 200)->nullable();
            $table->string('image1', 200)->nullable();
            $table->string('image2', 200)->nullable();
            $table->string('image3', 200)->nullable();
            $table->string('image4', 200)->nullable();
            $table->string('image5', 200)->nullable();
            $table->string('image6', 200)->nullable();
            $table->string('image7', 200)->nullable();
            $table->string('image8', 200)->nullable();
            $table->string('image9', 200)->nullable();
            $table->string('image10', 200)->nullable();
            $table->string('image11', 200)->nullable();
            $table->string('image12', 200)->nullable();
            $table->string('image13', 200)->nullable();
            $table->string('image14', 200)->nullable();
            $table->string('image15', 200)->nullable();
            $table->string('image16', 200)->nullable();
            $table->string('image17', 200)->nullable();
            $table->string('image18', 200)->nullable();
            $table->string('image19', 200)->nullable();
            $table->string('image20', 200)->nullable();
            $table->string('image21', 200)->nullable();
            $table->string('image22', 200)->nullable();
            $table->string('image23', 200)->nullable();
            $table->string('image24', 200)->nullable();
            $table->string('image25', 200)->nullable();
            $table->string('image26', 200)->nullable();
            $table->string('image27', 200)->nullable();
            $table->string('image28', 200)->nullable();
            $table->string('image29', 200)->nullable();
            
            
            $table->string('link0', 200)->nullable();
            $table->string('link1', 200)->nullable();
            $table->string('link2', 200)->nullable();
            $table->string('link3', 200)->nullable();
            $table->string('link4', 200)->nullable();
            $table->string('link5', 200)->nullable();
            $table->string('link6', 200)->nullable();
            $table->string('link7', 200)->nullable();
            $table->string('link8', 200)->nullable();
            $table->string('link9', 200)->nullable();
            $table->string('link10', 200)->nullable();
            $table->string('link11', 200)->nullable();
            $table->string('link12', 200)->nullable();
            $table->string('link13', 200)->nullable();
            $table->string('link14', 200)->nullable();
            $table->string('link15', 200)->nullable();
            $table->string('link16', 200)->nullable();
            $table->string('link17', 200)->nullable();
            $table->string('link18', 200)->nullable();
            $table->string('link19', 200)->nullable();
            $table->string('link20', 200)->nullable();
            $table->string('link21', 200)->nullable();
            $table->string('link22', 200)->nullable();
            $table->string('link23', 200)->nullable();
            $table->string('link24', 200)->nullable();
            $table->string('link25', 200)->nullable();
            $table->string('link26', 200)->nullable();
            $table->string('link27', 200)->nullable();
            $table->string('link28', 200)->nullable();
            $table->string('link29', 200)->nullable();

            // add those 2 colums to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();


            // use this column with the HasPosition trait
            $table->integer('position')->unsigned()->nullable();
        });

        // remove this if you're not going to use any translated field, ie. using the HasTranslation trait. If you do use it, create fields you want translatable in this table instead of the main table above. You do not need to create fields in both tables.
        Schema::create('page_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'page');
            
            $table->text('description')->nullable();

            $table->text('text0')->nullable();
            $table->text('text1')->nullable();
            $table->text('text2')->nullable();
            $table->text('text3')->nullable();
            $table->text('text4')->nullable();
            $table->text('text5')->nullable();
            $table->text('text6')->nullable();
            $table->text('text7')->nullable();
            $table->text('text8')->nullable();
            $table->text('text9')->nullable();
            $table->text('text10')->nullable();
            $table->text('text11')->nullable();
            $table->text('text12')->nullable();
            $table->text('text13')->nullable();
            $table->text('text14')->nullable();
            $table->text('text15')->nullable();
            $table->text('text16')->nullable();
            $table->text('text17')->nullable();
            $table->text('text18')->nullable();
            $table->text('text19')->nullable();
            $table->text('text20')->nullable();
            $table->text('text21')->nullable();
            $table->text('text22')->nullable();
            $table->text('text23')->nullable();
            $table->text('text24')->nullable();
            $table->text('text25')->nullable();
            $table->text('text26')->nullable();
            $table->text('text27')->nullable();
            $table->text('text28')->nullable();
            $table->text('text29')->nullable();

            $table->string('string0', 300)->nullable();
            $table->string('string1', 300)->nullable();
            $table->string('string2', 300)->nullable();
            $table->string('string3', 300)->nullable();
            $table->string('string4', 300)->nullable();
            $table->string('string5', 300)->nullable();
            $table->string('string6', 300)->nullable();
            $table->string('string7', 300)->nullable();
            $table->string('string8', 300)->nullable();
            $table->string('string9', 300)->nullable();
            $table->string('string10', 300)->nullable();
            $table->string('string11', 300)->nullable();
            $table->string('string12', 300)->nullable();
            $table->string('string13', 300)->nullable();
            $table->string('string14', 300)->nullable();
            $table->string('string15', 300)->nullable();
            $table->string('string16', 300)->nullable();
            $table->string('string17', 300)->nullable();
            $table->string('string18', 300)->nullable();
            $table->string('string19', 300)->nullable();
            $table->string('string20', 300)->nullable();
            $table->string('string21', 300)->nullable();
            $table->string('string22', 300)->nullable();
            $table->string('string23', 300)->nullable();
            $table->string('string24', 300)->nullable();
            $table->string('string25', 300)->nullable();
            $table->string('string26', 300)->nullable();
            $table->string('string27', 300)->nullable();
            $table->string('string28', 300)->nullable();
            $table->string('string29', 300)->nullable();
        });

        // remove this if you're not going to use slugs, ie. using the HasSlug trait
        Schema::create('page_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'page');
        });

        // remove this if you're not going to use revisions, ie. using the HasRevisions trait
        Schema::create('page_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'page');
        });
    }

    public function down()
    {
        Schema::dropIfExists('page_revisions');
        Schema::dropIfExists('page_translations');
        Schema::dropIfExists('page_slugs');
        Schema::dropIfExists('pages');
    }
}
