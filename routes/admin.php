<?php

// Register Twill routes here (eg. Route::module('posts'))
Route::group(['prefix' => 'home'], function () {
    Route::module('pages');
    Route::module('clientLogos');
});

Route::group(['prefix' => 'work'], function () {
	Route::module('pages');
    Route::module('projects');
    Route::module('gridSections');
    Route::module('services');
    Route::module('collaborators');
});

Route::group(['prefix' => 'services'], function () {
	Route::module('pages');
});

Route::group(['prefix' => 'about'], function () {
	Route::module('pages');
});

Route::group([], function () {
    Route::module('services');
    Route::module('collaborators');
    Route::module('gridSections');
});

Route::group(['prefix' => 'grid'], function () {
    Route::module('gridSections');
});

Route::group(['prefix' => 'taxonomies'], function () {
    Route::module('services');
    Route::module('collaborators');
});

Route::group(['prefix' => 'content'], function () {
    Route::module('gridSections');
    Route::module('products');
});