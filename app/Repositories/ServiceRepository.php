<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\Service;

class ServiceRepository extends ModuleRepository
{
    

    public function __construct(Service $model)
    {
        $this->model = $model;
    }
}
