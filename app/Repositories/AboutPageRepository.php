<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\AboutPage;

class AboutPageRepository extends ModuleRepository
{
    

    public function __construct(AboutPage $model)
    {
        $this->model = $model;
    }
}
