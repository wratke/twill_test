<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\Collaborator;

class CollaboratorRepository extends ModuleRepository
{
    

    public function __construct(Collaborator $model)
    {
        $this->model = $model;
    }
}
