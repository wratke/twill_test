<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\ClientLogo;

class ClientLogoRepository extends ModuleRepository
{
    

    public function __construct(ClientLogo $model)
    {
        $this->model = $model;
    }
}
