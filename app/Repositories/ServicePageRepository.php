<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\ServicePage;

class ServicePageRepository extends ModuleRepository
{
    

    public function __construct(ServicePage $model)
    {
        $this->model = $model;
    }
}
