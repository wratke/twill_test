<?php

namespace App\Repositories;


use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Project;

class ProjectRepository extends ModuleRepository
{
    
    use HandleBlocks; 

    public function __construct(Project $model)
    {
        $this->model = $model;
    }

    public function afterSave($object, $fields) {
	    $this->updateBrowser($object, $fields, 'services');
	    $this->updateBrowser($object, $fields, 'collaborators');
	    $this->updateBrowser($object, $fields, 'gridSections');
	    parent::afterSave($object, $fields);
	}

	public function getFormFields($object) {
	    // don't forget to call the parent getFormFields function
	    $fields = parent::getFormFields($object);

	    // get fields for a browser
	    $fields['browsers']['services'] = $this->getFormFieldsForBrowser($object, 'services');
	    $fields['browsers']['collaborators'] = $this->getFormFieldsForBrowser($object, 'collaborators');
	    $fields['browsers']['gridSections'] = $this->getFormFieldsForBrowser($object, 'gridSections');

	    // return fields
	    return $fields;
	}
}
