<?php

namespace App\Repositories;


use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\GridSection;

class GridSectionRepository extends ModuleRepository
{
    
    use HandleBlocks;

    public function __construct(GridSection $model)
    {
        $this->model = $model;
    }
}
