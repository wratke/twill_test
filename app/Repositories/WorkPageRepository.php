<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\WorkPage;

class WorkPageRepository extends ModuleRepository
{
    

    public function __construct(WorkPage $model)
    {
        $this->model = $model;
    }
}
