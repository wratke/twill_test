<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class AboutPageController extends ModuleController
{
    protected $moduleName = 'about_page';
}
