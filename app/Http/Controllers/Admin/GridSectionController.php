<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class GridSectionController extends ModuleController
{
    protected $moduleName = 'gridSections';

}
