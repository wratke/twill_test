<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class ServicePageController extends ModuleController
{
    protected $moduleName = 'service_page';
}
