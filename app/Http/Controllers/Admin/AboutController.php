<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class AboutController extends ModuleController
{
    protected $moduleName = 'about';
}
