<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class CollaboratorController extends ModuleController
{
    protected $moduleName = 'collaborators';
}
