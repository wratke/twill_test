<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class ClientLogoController extends ModuleController
{
    protected $moduleName = 'clientLogos';
}
