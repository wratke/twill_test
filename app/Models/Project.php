<?php

namespace App\Models;


use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Model;

class Project extends Model 
{

    use HasBlocks;
    

    protected $fillable = [
        'published',
        'title',
        'description',
        'overlay_title',
        'client',
        'highlighted_image',
        'associated_image',
        // 'position',
        // 'public',
        // 'featured',
        // 'publish_start_date',
        // 'publish_end_date',
    ];

    public function services()
    {
        return $this->belongsToMany(Service::class)->withPivot('position')->orderBy('position');
    }

    public function collaborators()
    {
        return $this->belongsToMany(Collaborator::class)->withPivot('position')->orderBy('position');
    }

    public function gridSections()
    {
        return $this->belongsToMany(GridSection::class)->withPivot('position')->orderBy('position');
    }

    // uncomment and modify this as needed if you use the HasTranslation trait
    // public $translatedAttributes = [
    //     'title',
    //     'description',
    //     'active',
    // ];
    
    // uncomment and modify this as needed if you use the HasSlug trait
    public $slugAttributes = [
        'title',
    ];

    // add checkbox fields names here (published toggle is itself a checkbox)
    public $checkboxes = [
        'published'
    ];

    // uncomment and modify this as needed if you use the HasMedias trait
    public $mediasParams = [
        'highlighted_image' => [
            'default' => [
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 4,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],
            ],
        ],
    ];
}
