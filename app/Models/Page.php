<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Model;

class Page extends Model 
{
    
    use HasMedias;
    use HasTranslation;

    protected $fillable = [
        'published',
        'title',
        'description',
        // 'position',
        // 'public',
        // 'featured',
        // 'publish_start_date',
        // 'publish_end_date',
    ];

    // uncomment and modify this as needed if you use the HasTranslation trait
     public $translatedAttributes = [
         'title',
         'description',
         'text0',
         'text1',
         'text2',
         'text3',
         'text4',
         'text5',
         'text6',
         'text7',
         'text8',
         'text9',
         'text10',
         'text11',
         'text12',
         'text13',
         'text14',
         'text15',
         'text16',
         'text17',
         'text18',
         'text19',
         'text20',
         'text21',
         'text22',
         'text23',
         'text24',
         'text25',
         'text26',
         'text27',
         'text28',
         'text29',
         'string0',
         'string1',
         'string2',
         'string3',
         'string4',
         'string5',
         'string6',
         'string7',
         'string8',
         'string9',
         'string10',
         'string11',
         'string12',
         'string13',
         'string14',
         'string15',
         'string16',
         'string17',
         'string18',
         'string19',
         'string20',
         'string21',
         'string22',
         'string23',
         'string24',
         'string25',
         'string26',
         'string27',
         'string28',
         'string29',
     ];
    
    // uncomment and modify this as needed if you use the HasSlug trait
    // public $slugAttributes = [
    //     'title',
    // ];

    // add checkbox fields names here (published toggle is itself a checkbox)
    public $checkboxes = [
        'published'
    ];

    // uncomment and modify this as needed if you use the HasMedias trait
    public $mediasParams = [
         'header_img' => [
             'default' => [
                 [
                     'name' => 'landscape',
                     'ratio' => 16 / 9,
                 ],
             ],
         ],
    ];
}
